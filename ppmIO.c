//ppmIO.c
//Debanik Purkayastha (dpurkay1) & Jerry Yao (jyao14)

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageManip.h"


/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {

  // Initialize an image pointer and allocate memory for the pointer
  Image *img;
  img = (Image*)malloc(sizeof(Image));

  // Initialize a character and an integer that reads the rgb value of the ppm file
  char ch;
  int rgb;

  // Check to see if the ppm file numbers equal a ppm
  if (fscanf(fp, "P%c\n", &ch) != 1 || ch != '6') {
    return NULL;
  } 

  // While loop to get the characters
  ch = getc(fp);
  while (ch == '#') {
    while (ch != '\n') {
      ch = getc(fp);
    }
      ch = getc(fp);
  }

  ungetc(ch, fp);

  // Initialize the dimensions and the rgb of the ppm file
  int dimensions_returned = fscanf(fp, "%d %d", &img->cols, &img->rows);
  int rgb_returned = fscanf(fp, "%d", &rgb);
  
  // Check the dimensions and the rgb of the ppm file
  if (dimensions_returned != 2 || rgb_returned != 1) {
    return NULL;
  }

  // Check the rgb value to see if it equals 225
  if (rgb != 255) {
    return NULL;
  }

  while (fgetc(fp) != '\n');

  // Allocate memory for the pixels of the image
  img->data = (Pixel*)malloc(img->rows * img->cols * sizeof(Pixel));

  int el_collected = fread(img->data, 3 * img->rows, img->cols, fp);  

  // Check to see if el_collected equals the cols of the PPM file
  if (el_collected != (img->cols)) {
    return NULL;
  }

  fclose(fp);
  return img;
}




/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }

  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  /* write pixels */
  int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    return -1;
  }

  /* success, so return number of pixels written */
  return num_pixels_written;
}




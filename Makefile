# Debanik Purkayastha (dpurkay1) & Jerry Yao (jyao14)
# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CONSERVATIVE_FLAGS = -std=c99 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

# Links together files needed to create executable
project: project.o ppmIO.o commandUtil.o imageManip.o
	$(CC) -o project project.o ppmIO.o commandUtil.o imageManip.o

# Compiles hw3.c to create hw3.o
project.o: project.c ppmIO.h commandUtil.h imageManip.h
	$(CC) $(CFLAGS) -c project.c

# Compiles dnasearch.c into an object file
ppmIO.o: ppmIO.c ppmIO.h 
	$(CC) $(CFLAGS) -c ppmIO.c

# Compiles dnasearch.c into an object file                                                                                                                                       
imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

# Compiles dnasearch.c into an object file
commandUtil.o: commandUtil.c commandUtil.h
	$(CC) $(CFLAGS) -c commandUtil.c

# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o project *.gcov

//Debanik Purkayastha (dpurkay1) & Jerry Yao (jyao14)     

#include "ppmIO.h"

// The headers for the functions 
int swap(Image* img);
int blackout(Image* img);
int grayscale(Image* img);
Image* crop(Image* img, int xtop, int ytop, int xbottom, int ybottom);
int contrast(Image* img, double adj);
unsigned char process_rgb(unsigned char val, double adj_factor);

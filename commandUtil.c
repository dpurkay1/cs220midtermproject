//Debanik Purkayastha (dpurkay1) & Jerry Yao (jyao14)     
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"
#include "string.h"

// This function reads the argument and performs the operation function on the PPM for whatever operation is given
int readInput(int argc, char** argv) {

  // Check to see if the input file name or if the output file name is supplied
  if (argv[1] == NULL) {
    fprintf(stderr, "%s","Failed to supply input file name\n");
    return 1;
  } else if (argv[2] == NULL) {
    fprintf(stderr, "%s","Failed to supply output file name\n");
    return 1;
  }

  // Create a file pointer to the input PPM file and read the PPM file
  FILE *f = fopen(argv[1], "rb");
  if (f == NULL) {
    fprintf(stderr, "%s", "Input file could not be found\n");
    return 2;
  }

  Image* img = readPPM(f);

  // Check to see if the user inputed a file
  if (img == NULL) {
    fprintf(stderr, "%s","PPM file formatted incorrectly\n");
    return 3;
  }

  // Check to see if the user inputed a operation
  if (argv[3] == NULL) {
    fprintf(stderr, "%s","No operation specified\n");
    return 5;
  }

  // Set of functions based on the given operation after the output file name
  if (strcmp(argv[3], "swap") == 0) {
    swap(img);
  } else if (strcmp(argv[3], "blackout") == 0) {
    blackout(img);
  } else if (strcmp(argv[3], "grayscale") == 0) {
    grayscale(img);
  } else if (strcmp(argv[3], "crop") == 0) {
    if (argc != 8) {
      fprintf(stderr, "%s", "Please specify the specific arguments for the crop command\n");
      return 6;
    }
    // Check to see if the arguments exist for the crop command
    if (argv[4] == NULL || argv[5] == NULL || argv[6] == NULL || argv[7] == NULL) {
      fprintf(stderr, "%s", "Please specify the specific arguments for the crop command\n");
      return 6;
    }
    // Check to make sure that the crop top left corners are not greater than the bottom write corners
    if (atoi(argv[4]) > atoi(argv[6]) || atoi(argv[5]) > atoi(argv[7])) {
      fprintf(stderr, "%s", "Incorrect crop command arguments\n");
      return 7;
    }

    // Check to see user image is in range of crop arguments                                                                                                                                               
    if (atoi(argv[6]) > img->rows || atoi(argv[7]) > img->cols) {
      fprintf(stderr, "%s", "Crop command arguments out of range\n");
      return 7;
    }
    
    Image* img_crop = crop(img, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
    FILE *f_crop = fopen(argv[2], "wb");

    // Check to see if the output file exists
    if (writePPM(f_crop, img_crop) == -1) {
      fprintf(stderr, "%s", "Specified output file could not be opened for writing\n");
      return 4;
    }
    free(img->data);
    free(img);
    free(img_crop->data);
    free(img_crop);
    return 0;
  } else if (strcmp(argv[3], "contrast") == 0) {
    // Check to make sure there is a contrast specifier for the contrast command
    if (argc != 5) {
      fprintf(stderr, "%s", "Please specify the specific arguments for the contrast  command\n");
      return 6;
    }
    if (argv[4] == NULL) {
      fprintf(stderr, "%s","Please specify the specific argument for the contrast commant\n");
      return 6;
    }
    double factor = atof(argv[4]);
    // Check to see if the factor inputed is a string or the 0 value
    if (factor == 0.0) {
      fprintf(stderr, "%s", "Incorrect argument for contrast\n");
      return 6;
    }
    contrast(img, factor);
  } else {
    // Check to see if the arguments match the command
    fprintf(stderr, "%s","Command not supported\n");
    return 5;
  }

  // Create a file pointer to the output file given by the user and write a file
  FILE *f1 = fopen(argv[2], "wb");

  // Check to see if an output file could be written
  if ((writePPM(f1, img)) == -1) {
    fprintf(stderr, "%s", "Specified output file could not be opened for writing\n");
    return 4;
  }

  // Freeing the data to prevent memory leaks
  free(img->data);
  free(img);

  return 0;
}

//Debanik Purkayastha (dpurkay1) & Jerry Yao (jyao14)

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"

// This is the swap command to swap the RGB values of the PPM file
int swap(Image* img) {
  // Initialze the rows and cols to the rows and cols of the image struct
  int rows = img->rows;
  int cols = img->cols;

  // Nested for loop that sets the sets the green value to red, blue to green, and red to blue
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      unsigned char r_temp = img->data[r*cols + c].r;
      img->data[r*cols + c].r = img->data[r*cols + c].g;
      img->data[r*cols + c].g = img->data[r*cols + c].b;
      img->data[r*cols + c].b = r_temp;;
    }
  }
  return 0;
}

// This is the blackout command that sets all of the RGB values to for a certain function
int blackout(Image* img) {
  // Initialize the rows and cols to the rows and cols of the image struct
  int rows = img->rows;
  int cols = img->cols;

  // Nested For loop that sets the pixel data to 0 the top right quadrant
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      if ((r < (rows / 2)) && (c >= ((cols+1)/ 2))) {
	img->data[r*cols + c].r = 0;
	img->data[r*cols + c].g = 0;
	img->data[r*cols + c].b = 0;
      }
    }
  }

  return 0;
}

// This is the crop command that crops part of original image and sets it as the new image
Image* crop(Image* img, int xtop, int ytop, int xbottom, int ybottom) {
  // Initialize the differences for the rows(x) and cols(y)
  int x_diff = xbottom - xtop;
  int y_diff = ybottom - ytop;

  // Initialize the cols to the image cols
  int cols = img->cols;

  // Create a new image that will be the cropped image
  Image *cropimg;

  // Free memory for the new cropped image and set the rows and cols to the differences
  cropimg = (Image*)(malloc(sizeof(Image)));
  cropimg->rows = y_diff;
  cropimg->cols = x_diff;

  // Free memory for the pixel data
  cropimg->data = (Pixel*)(malloc(x_diff * y_diff * sizeof(Pixel)));

  // Nested for loop that sets the part of the original image to the new image
  for (int r = 0; r < y_diff; r++) {
    for (int c = 0; c < x_diff; c++) {
      cropimg->data[r*x_diff + c].r = img->data[(r+ytop)*cols + (c+xtop)].r;
      cropimg->data[r*x_diff + c].g = img->data[(r+ytop)*cols + (c+xtop)].g;
      cropimg->data[r*x_diff + c].b = img->data[(r+ytop)*cols + (c+xtop)].b;
    }
  }
  
  return cropimg;
}

// This is the definition for the grayscale function that sets the ppm image to gray
int grayscale(Image* img) {

  // Initialize the rows and cols to the image rows and cols
  int rows = img->rows;
  int cols = img->cols;

  // Nested for loop that first creates a gray value and sets all of the pixels to that value
  for (int r = 0; r < rows; r++){
    for (int c = 0; c < cols; c++) {
      unsigned char gray = (unsigned char) ((0.30*(img->data[r*cols + c].r))+(0.59*(img->data[r*cols + c].g))+(0.11*(img->data[r*cols + c].b)));
        img->data[r*cols + c].r = gray;
        img->data[r*cols + c].g = gray;
        img->data[r*cols + c].b = gray;
    }
  }

  return 0;									
}

// This is a function that creates an adjusted character for the contrast command 
unsigned char process_rgb(unsigned char val, double adj_factor) {
  // Initializes the conversion value and the adjustment value
  double conv_val = ((double)val / 255) - 0.5;
  double adjust_double = conv_val * adj_factor;

  // Initializes the adjustment character 
  double adjust_char = (255 * (adjust_double + 0.5));

  // If statements that check the adjustment to see if it's above 225 or equal to 0
  if (adjust_char > 255) {
    adjust_char = 255;
  }

  if (adjust_char < 0) {
    adjust_char = 0;
  }

  return (unsigned char)adjust_char;
}

// This is the definition for contrast that uses the process_rgb function
int contrast(Image* img, double adjustment) {

  // Inititalizes the rows and cols to the image rows and cols
  int rows = img->rows;
  int cols = img->cols;

  // Nested for loop that sets the pixels to the adjusted values
  for (int r = 0; r < rows; r++) {
    for (int c = 0; c < cols; c++) {
      img->data[r*cols + c].r = process_rgb(img->data[r*cols + c].r, adjustment);
      img->data[r*cols + c].g =	process_rgb(img->data[r*cols + c].g, adjustment);
      img->data[r*cols + c].b =	process_rgb(img->data[r*cols + c].b, adjustment);
    }
  }
  return 0;
}
